

int endStop1 = 2;
int endStop2 = 3;

int motorPin1 = 4;
int motorPin2 = 5;

void setup() {
  Serial.begin(115200);

  Serial.println("starting");
  
  pinMode(endStop1, INPUT_PULLUP);
  pinMode(endStop2, INPUT_PULLUP);

  pinMode(motorPin1,OUTPUT);
  pinMode(motorPin2,OUTPUT);
  
  forward();
}

void loop() {

  if (digitalRead(endStop1) == LOW) {
    forward();
  }

  if (digitalRead(endStop2) == LOW) {
    reverse();
  }

}



void forward() {
  digitalWrite(motorPin1, HIGH);
  digitalWrite(motorPin2, LOW);

  Serial.println("going forward");
}


void reverse() {
  digitalWrite(motorPin1, LOW);
  digitalWrite(motorPin2, HIGH);

  Serial.println("going backwards");
}

