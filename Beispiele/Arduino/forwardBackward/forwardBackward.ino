
int motorPin1 = 2;
int motorPin2 = 3;

int motorPin3 = 4;
int motorPin4 = 5;


void setup() {
  Serial.begin(115200);

  Serial.println("starting");

  pinMode(motorPin1,OUTPUT);
  pinMode(motorPin2,OUTPUT);

  pinMode(motorPin3,OUTPUT);
  pinMode(motorPin4,OUTPUT);
}

void loop() {

  
    forward();
    delay(200);
    reverse();
    delay(200);

}



void forward() {
  digitalWrite(motorPin1, HIGH);
  digitalWrite(motorPin2, LOW);

  Serial.println("going forward");
}


void reverse() {
  digitalWrite(motorPin1, LOW);
  digitalWrite(motorPin2, HIGH);

  Serial.println("going backwards");
}

