


int sensorPin = 3;

void setup() {

  Serial.begin(115200);

  pinMode(sensorPin,INPUT_PULLUP);
  
}

void loop() {

  boolean pinStatus = digitalRead(sensorPin);

  Serial.println(pinStatus);
  
  delay(100);
  
}
