let five = require("johnny-five");
let board = new five.Board();


let interval = 500;
board.on("ready", function () {

    let direction = true

    let motor = new five.Motor({
        pins: {
            dir: 4,
            cdir: 5,
            pwm: 9
        }
    });

    motor.on("forward", function () {
        console.log(motor.direction, Date.now());

        board.wait(interval, function () {
            motor.reverse(50);
        });
    });

    motor.on("reverse", function () {
        console.log(motor.direction, Date.now());

        board.wait(interval, function () {
            motor.forward(255);
        });
    });

    motor.forward(255);
});


    // let sensor = new five.Button(9)

    // sensor.on('up', () => {
    //     console.log('high')
    // });

    // sensor.on('down', () => {
    //     console.log('low')
    // });